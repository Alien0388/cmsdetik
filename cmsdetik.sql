-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2018 at 05:25 PM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmsdetik`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`id`, `username`, `PASSWORD`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `tblnews`
--

CREATE TABLE `tblnews` (
  `id_news` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `focus` tinyint(1) DEFAULT NULL,
  `content` text,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `writer` varchar(255) DEFAULT NULL,
  `view` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblnews`
--

INSERT INTO `tblnews` (`id_news`, `title`, `focus`, `content`, `date`, `writer`, `view`) VALUES
(1, ' Rombongan SMK Tangerang Kesurupan Usai dari Candi Prambanan', 0, 'Yogyakarta - Rombongan study tour dari SMK 4 Tangerang, Banten mengalami kesurupan massa di Yogyakarta. Mereka baru saja mengunjungi obyek wisata Candi Prambanan.\r\n\r\nRombongan mengalami kesurupan saat berada di dekat Jl Veteran Umbulharjo, selatan simpang empat Sari Husada, Kota Yogyakarta. Tiba-tiba salah satu siswa yang ada di bus berteriak histeris. Siswa yang lain di dalam bus kemudian juga ada yeng ikut histeris dan berteriak-teriak saat turun dari bus.\r\n\r\nSalah satu siswa yang mengalami kesurupan pun tiba-tiba bisa berbahasa Jawa. Padahal sebelumnya tidak bisa berbahasa Jawa. Siswa tersebut berteriak seperti marah-marah agar mengembalikan kain selendang.\r\n\r\n"Balekno lendange (kembalikan selendangnya)," kata salah satu siswa.\r\n\r\nGuru pendamping bersama warga kemudian memisahkan siswa yang kesurupan dengan siswa tidak mengalami kesurupan. Di sekitar Jl Veteran kemudian dipenuhi warga. Aparat kepolisian dari Polresta Yogyakarta langsung mengamankan sekitar lokasi dengan menutup jalan.\r\n\r\nBeberapa orang kemudian ikut membantu menyembuhkan. Bahkan ada yang membantu ikut mencari kain selendang seperti yang diomongkan salah satu siswa yang kesurupan. Seorang juru kunci dari Candi Prambanan juga didatangkan.\r\n\r\nSetelah ditemukan, kain yang dibawa oleh salah seorang untuk dikembalikan ke Prambanan. Siswa yang masih berteriak histeris turut serta dibawa ke Prambanan menggunakan mobil ambulans dikawal aparat kepolisian.\r\n\r\nSetelah situasi tenang, semua siswa kemudian diminta masuk kembali ke dalam bus untuk menuju hotel. Bus pun kemudian meninggalkan lokasi tersebut.\r\nBaca juga: Menteri Koperasi Bernostalgia di Candi Prambanan\r\n\r\n"Ada yang mengambil kain selendang atau penutup batu candi. Sudah dibawa ke Prambanan lagi, semua sudah tenang," kata Hidayat salah satu warga Umbulharjo yang turut mengobati dan menenangkan siswa.\r\nBaca juga: Siswa SMP di Bangka ''Langganan'' Kesurupan Massal Sepekan Terakhir\r\n\r\nSelama lebih kurang 90 menit untuk menenangkan siswa yang kesurupan sejak pukul 21.15 hingga pukul 23.00 WIB. Lebih dari 6 bus dalam rombongan study tour tersebut. ', '2018-04-08 15:08:49', 'Robert A', 16),
(4, 'OTT Bupati di Jabar, 6 Orang Tiba di KPK', 1, 'Jakarta - Enam orang yang terjaring dalam operasi tangkap tangan di Jawa Barat sudah tiba di gedung KPK. Mereka kini menjalani pemeriksaan intensif.\r\n\r\n"Enam orang tadi sudah dibawa ke kantor KPK. Dilanjutkan proses pemeriksaan secara intensif," ungkap Kabiro Humas KPK Febri Diansyah kepada wartawan, Rabu (11/4/2018).\r\n\r\nNamun Febri tidak menyebut adanya bupati aktif dari keenam orang yang sudah tiba tersebut. Mereka adalah kepala dinas dan PNS.\r\n\r\nBaca juga: KPK Amankan 7 Orang dalam OTT Bupati di Jawa Barat\r\n\r\n"Enam orang dari unsur kepala dinas dan PNS," tuturnya. \r\n\r\nFebri menjelaskan pemeriksaan itu dilakukan dalam jangka waktu maksimal 24 jam untuk menentukan status hukum keenamnya, termasuk proses kasus yang terjadi.\r\n\r\nBaca juga: KPK Sita Uang Ratusan Juta Rupiah Saat OTT Bupati di Jawa Barat\r\n\r\n"Nanti hasilnya tentu akan kami sampaikan di konferensi pers," kata dia lagi.\r\n\r\nSebelumnya, KPK memberi konfirmasi adanya OTT di salah satu kabupaten di Jabar. Dari tujuh orang yang sebelumnya diamankan, ada seorang bupati aktif. Selain itu, KPK mengamankan uang ratusan juta rupiah dalam operasi senyap tersebut.\r\n\r\nBaca juga: Bupati Bandung Barat Jumpa Pers Akui Didatangi KPK\r\n\r\n"Ada uang yang diamankan. Ratusan juta rupiah," ujar Febri kepada wartawan, Selasa (10/4). ', '2018-04-10 12:28:35', 'Robert A', 15),
(5, 'Underpass Mampang Dibuka Hari ini Pukul 06.00 WIB', 0, 'Jakarta - Underpass Mampang-Kuningan mulai beroperasi hari ini pukul 06.00 WIB. Wakil Gubernur DKI Sandiaga Uno berharap kondisi underpass Mampang-Kuningan tidak seperti underpass Matraman.\r\n\r\n"Besok itu akan ada underpass Mampang. Tadi saya lewat juga. Jadi di underpass Mampang insyaallah lebih simpel karena sekarang tidak mengubah arus," kata Sandiaga di Jalan Pramuka, Jakarta Pusat, Selasa (10/4/2018).\r\n\r\nNantinya, petugas dari Dinas Perhubungan DKI juga akan siaga di sekitar underpass untuk memantau situasi dan kondisi.\r\nBaca juga: Menanti Keampuhan Underpass Mampang-Kuningan\r\n\r\n"Underpass Mampang kita bukan jam 06.00 WIB. Tapi petugas Dishub sudah bersiaga sekitar pukul 05.00 WIB," tutur Kadishub DKI Andri Yansya di lokasi yang sama.\r\n\r\nUnderpass Mampang-Kuningan direncanakan dibuka pada 7 April 2018. Namun batal dilakukan karena traffic light belum tersedia.\r\n\r\n"Untuk pengoperasian lintas bawah Mampang-Kuningan perlu dilaksanakan penataan traffic management dan penataan traffic light di kawasan sekitar lokasi, terutama pada simpang Mampang dan simpang Kuningan," ujar Kepala Bidang Simpang dan Jalan Tak Sebidang Dinas Bina Marga DKI Heru Suwondo dalam keterangan tertulis yang diterima detikcom, Jumat (6/4).', '2018-04-10 18:41:19', 'Bayu S', 0),
(7, 'Ide Liburan ala Puteri Indonesia 2018 di Belitung', 1, 'Belitung - Puteri Indonesia 2018 asal Belitung, Sonia Fergina, melepas rindu ke kampung halamannya pekan lalu. Ia mengunjungi sejumlah destinasi wisata di sana.\r\n\r\nSonia Fergina pulang ke Belitung dalam acara ''Sonia Melepas Rindu'' pada Sabtu (7/4) kemarin. Selama di Belitung, dara cantik berusia 25 tahun ini mengikuti tradisi adat setempat sekaligus wisata ke berbagai destinasi yang ada di sana.\r\n\r\nDia pun mengunggah keseruannya di Belitung ke akun Instagramnya, @soniafergina, seperti dilihat detikTravel, Rabu (11/4/2018). Salah satu foto yang diunggah menampilkan Sonia sedang menghadiri 1.000 Dulang, yakni tradisi makan bersama.\r\n\r\nSonia pun juga berkunjung ke pantai. Wisata Belitung memang tak lengkap tanpa menikmati keindahan pantainya. Di Pantai Tanjung Kelayang,Sonia sempat melakukan pelepasan tukik atau anak penyu sisik. Nah, Tanjung Kelayang memang termasuk favorit turis. Pantai ini membentang sepanjang 4 km dengan hamparan pasir putih dan batu granit besar khas Belitung. Selama wisata pantai,traveler bisa berenang, hunting foto, menikmatisunset, maupunislandhopping ke pulau sekitarnya seperti Pulau Lengkuas. BACA JUGA: Rute Asyik Jelajah Belitung 3 Hari 2 Malam https://travel.detik.com/domestic-destination/d-2979052/rute-asyik-jelajah-belitung-3-hari-2-malam Wisata ke satu pantai rasanya kurang lengkap. Ada Pantai Tanjung Tinggi yang tak kalah keren.Sonia asyik main air dan foto-foto di pantai yang menjadi lokasishooting film Laskar Pelangi dan Sang Pemimpi ini. "Pantai Tanjung Tinggi sebagai salah satu ikon wisata yang ada di Belitung merupakan lokasishooting film Laskar Pelangi dan Sang Pemimpi yang biasanya dikenal sebagai sebutan pantai Laskar Pelangi," kataSonia.', '2018-04-11 14:55:58', 'Bayu S', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `tblnews`
--
ALTER TABLE `tblnews`
  ADD PRIMARY KEY (`id_news`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbladmin`
--
ALTER TABLE `tbladmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblnews`
--
ALTER TABLE `tblnews`
  MODIFY `id_news` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
