<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    function __construct(){
        parent::__construct();
        //if($this->session->userdata('status_login')==TRUE){

        //}
        //else{
          //  redirect(base_url().'index.php/login/');
	   //}
    }

    public function leftside(){
        $this->load->view('template/leftside');
    }

    public function header(){
        $this->load->view('template/header');
    }

    public function berita(){
      if($this->session->userdata('status_login')==TRUE){
		        $this->load->view('template/head');
              $this->leftside();
              $this->header();
            }
      else{
			     redirect(base_url().'index.php/login/');
		  }
      $data['list'] = $this->Berita_model->get_all();
      $this->load->view('admin/admin',$data);
    }

    public function form_berita(){
      if($this->session->userdata('status_login')==TRUE){
            $data['action'] = base_url().'index.php/admin/add_berita';
		        $this->load->view('template/head');
              $this->leftside();
              $this->header();
            }
      else{
			     redirect(base_url().'index.php/login/');
		  }
      $this->load->view('admin/form_berita',$data);
    }

	public function add_berita(){
	   $post = $this->input->post();
	    $this->Berita_model->insert($post);
      redirect(base_url().'index.php/admin/berita');
	}

  public function delete_berita($id_news){
    $this->Berita_model->delete($id_news);

		redirect(base_url().'index.php/admin/berita');
  }

  public function edit_berita($id_news) {
    if($this->session->userdata('status_login')==TRUE){
          $this->load->view('template/head');
            $this->leftside();
            $this->header();
          }
    else{
         redirect(base_url().'index.php/login/');
    }
		$data['action'] = base_url().'index.php/admin/proses_perbarui_berita/'.$id_news;
		$data['berita'] = $this->Berita_model->get_berita_by($id_news);

		$this->load->view('admin/form_berita', $data);

	}

	public function proses_perbarui_berita($id_news) {
		$post = $this->input->post();

		if ($this->Berita_model->update_berita($post, $id_news)) {
			redirect(base_url().'index.php/admin/berita');
		}
	}
}