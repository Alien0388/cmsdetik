<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita extends CI_Controller {

	public function index(){
		$this->load->view('template/head');
		$data['list'] = $this->Berita_model->get_all();
		$data['most'] = $this->Berita_model->get_most();
		$data['focus'] = $this->Berita_model->get_focus();
		$this->load->view('berita',$data);
    }

  public function content($id){
    $this->load->view('template/head');
		$data['list'] = $this->Berita_model->get_content($id);
		$data['most'] = $this->Berita_model->get_most();
		$data['focus'] = $this->Berita_model->get_focus();
		$this->load->view('berita/content_berita',$data);

		//tambah viewer
		$this->Berita_model->viewplus($id);
  }
}