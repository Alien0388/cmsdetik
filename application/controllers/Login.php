<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index(){
        if(isset($_POST['submit'])){
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $hasil = $this->Login_model->login($username,md5($password));

            if($hasil > 0){
               $this->session->set_userdata(array(
                    'username' => $username,
					'status_login' => TRUE));
                redirect(base_url()."index.php/admin/berita");
            }
            else{
                $this->session->set_flashdata('gagal', 'Login Failed, Please check your username/password');
                $this->load->view('login/login');
            }
        }
        else{
            //$this->session->set_flashdata('gagal', 'Kosong');
            $this->load->view('login/login');
        }
    }

    public function logout(){
        session_destroy();
        redirect(base_url()."index.php/login");
    }
}