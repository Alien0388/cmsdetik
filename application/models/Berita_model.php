<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Berita_model extends CI_Model {

    function get_all() {
        $this->db->select('*');
        $this->db->from('tblnews');
        $this->db->order_by('id_news', 'desc');
        $query = $this->db->get();

		return $query->result_array();
	}

  function get_most() {
      $query = $this->db->query("SELECT * FROM tblnews
        WHERE view=( SELECT max(view) FROM tblnews )");

  return $query->result_array();
}

function get_focus() {
    $query = $this->db->query("SELECT * FROM tblnews
      WHERE focus = 1");

return $query->result_array();
}

  function get_berita_by($id_news){
    $this->db->select('*');
    $this->db->from('tblnews');
    $this->db->where('id_news',$id_news);
    $query = $this->db->get();

    return $query->row_array();
  }

  function get_content($id) {
      $this->db->select('*');
      $this->db->from('tblnews');
      $this->db->where('id_news', $id);
      $query = $this->db->get();

  return $query->result_array();
  }

  function insert($post) {
		$data = array(
			'title'			=> $post['title'],
			'content' 	=> $post['content'],
      'focus'    => $post['focus'],
      'writer'    => $post['writer'],
			);

		if ($this->db->insert('tblnews', $data)) {
			return true;
		} else {
			return false;
		}
	 }

   function update_berita($post, $id_news){
		$this->db->where('id_news', $id_news);

		$data = array(
      'title'			=> $post['title'],
			'content' 	=> $post['content'],
      'focus'    => $post['focus'],
      'writer'    => $post['writer'],
			);

		if ($this->db->update('tblnews', $data)) {
			return true;
		} else {
			return false;
		}
	}

   function delete($id_news){
     $this->db->where('id_news', $id_news);

		if ($this->db->delete('tblnews')) {
			return true;
		} else {
			return false;
		}
   }

   function viewplus($id_news){
     $this->db->query("UPDATE tblnews SET view = view + 1 WHERE id_news = '".$id_news."'");
   }
}
