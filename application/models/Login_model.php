<?php
class Login_model extends CI_Model{
	
    public function login($username,$password){
        $check = $this->db->get_where('tbladmin',array('username'=>$username, 'password'=>$password));
        
        if($check->num_rows()>0){
            return 1;
        }
        else{
            return 0;
        }
    }
    
    function get_user_privilege($user){
		$sql = "SELECT * FROM tbladmin WHERE username=?"; 
		$query = $this->db->query($sql, array($user));		
		return $query->row()->role;
	}

}