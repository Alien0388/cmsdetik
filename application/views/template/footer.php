<footer class="main-footer">
    <div class="pull-right hidden-xs"> <b>Version</b> 1.0.0 </div> <strong> <a href="#"></a></strong>  </footer>
<aside class="control-sidebar control-sidebar-dark">
    <div class="tab-content">
        <div class="tab-pane" id="control-sidebar-home-tab"> </div>
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <div class="tab-pane" id="control-sidebar-settings-tab"> </div>
    </div>
</aside>
<div class="control-sidebar-bg"></div>
</div>


<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.15/sorting/date-eu.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script src="<?php echo base_url().'assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js'?>"></script>
<script src="<?php echo base_url().'assets/adminlte/dist/js/app.min.js'?>"></script>

<script>
$.fn.dataTable.ext.search.push(
function( settings, data, dataIndex ) {
  var min = parseInt( $('#min').val(), 10 );
  var max = parseInt( $('#max').val(), 10 );
  var age = parseFloat( data[3] ) || 0; // use data for the age column

  if ( ( isNaN( min ) && isNaN( max ) ) ||
       ( isNaN( min ) && age <= max ) ||
       ( min <= age   && isNaN( max ) ) ||
       ( min <= age   && age <= max ) )
  {
      return true;
  }
  return false;
}
);

$(document).ready(function() {
var table = $('#example7').DataTable({
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    return 'Details for '+data[0]+' '+data[1];
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll()
        }
    }
});

// Event listener to the two range filtering inputs to redraw on input
$('#min, #max').keyup( function() {
  table.draw();
} );
} );
</script>

<script>
$(document).ready(function() {
$('#example1').DataTable( {

    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.modal( {
                header: function ( row ) {
                    var data = row.data();
                    return 'Details for '+data[0]+' '+data[1];
                }
            } ),
            renderer: $.fn.dataTable.Responsive.renderer.tableAll()
        }
    }
} );
} );
    $(function () {
        $("#example2").DataTable();
    });
    $(function () {
        $("#example3").DataTable();
    });
    $(function () {
        $("#example4").DataTable();
    });
    $(function () {
        $(".example5").DataTable({
         columnDefs: [
       { type: 'date-eu', targets: 1 }
     ],
            "order": [[ 1, "desc" ]]
        });
    });

</script>
<script>
	        $(function () {
	            $('.example8').DataTable({
                    responsive: true
                });
	            $('.example2').DataTable({
	                "paging": true
	                , "lengthChange": false
	                , "searching": false
	                , "ordering": true
	                , "info": true
	                , "autoWidth": false
	            });
	        });
	    </script>
<script>
  $(function () {
    //Add text editor
    $("#compose-textarea").wysihtml5();
  });
</script>
<script>
$(document).ready(function(){
   $(this).find('img').attr('alt');
});
</script>

</body>

</html>
