 <!-- =============================================== -->
        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image"> <img src="https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> </div>
                    <div class="pull-left info">
                        <p><?php echo $this->session->userdata("username")?><p>
                        <a href="#"> </a> </div>
                </div>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">Menu</li>
                    <li class="treeview">
                                  <a href="<?php echo base_url();?>">
                                      <i class="fa fa-dashboard"></i> <span>View Page</span>
                                      <span class="pull-right-container">
                                          <small class="label pull-right bg-green"></small>
                                      </span>
                                  </a>

                              </li>
                    <li class="treeview <?php if($this->uri->segment(2) == "berita"){ echo "active";}?>">
                        <a href="<?php echo base_url();?>index.php/admin/berita">
                            <i class="fa fa-newspaper-o"></i> <span>Berita</span>
                            <span class="pull-right-container">
                                <small class="label pull-right bg-green"></small>
                            </span>
                        </a>

                    </li>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>
