<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
Form Berita
</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#"><?php echo ucfirst($this->uri->segment(1))?></a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Input Berita</h3> </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="col-md-12">
                            <!-- general form elements -->
                            <div class="box box-primary">
                                <!-- /.box-header -->
                                <!-- form start -->
                                <form action="<?php echo $action?>" method="post">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label>Title</label>
                                            <input type="text" class="form-control" name="title" placeholder="Title" value="<?php echo (isset($berita['title'])) ? $berita['title'] : ''; ?>"> </div>
                                        <div class="form-group">
                                            <label>Content</label>
                                            <textarea class="form-control" rows="3" name="content" placeholder="Content">
                                                <?php echo (isset($berita['content'])) ? $berita['content'] : ''; ?>
                                            </textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Focus</label>
                                            <select name="focus" class="form-control">
                                                <option value="<?php echo (isset($berita['focus'])) ? $berita['focus'] : ''; ?>" selected>
                                                    <?php echo (isset($berita['focus'])) ? $berita['focus'] : ''; ?>
                                                </option>
                                                <option value="1">Yes</option>
                                                <option value="0">No</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Writer</label>
                                            <select name="writer" class="form-control">
                                                <option value="<?php echo (isset($berita['writer'])) ? $berita['writer'] : ''; ?>" selected>
                                                    <?php echo (isset($berita['writer'])) ? $berita['writer'] : ''; ?>
                                                </option>
                                                <option value="Bayu S">Bayu S</option>
                                                <option value="Robert A">Robert A</option>
                                            </select>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-8"> </div>
                                            <!-- /.col -->
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                                            </div>
                                            <!-- /.col -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('template/footer.php')?>