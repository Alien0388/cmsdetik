<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
Berita
</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-newspaper-o"></i> Berita</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Berita</h3>
                        <a href="<?php echo base_url().'index.php/admin/form_berita'?>">
                            <button class="btn btn-info pull-right">Add New</button>
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="example4" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>Focus</th>
                                        <th>Writer</th>
                                        <th>View</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($list as $value){?>
                                        <tr>
                                            <td>
                                                <?php echo $value['title']?>
                                            </td>
                                            <td>
                                                <?php echo $value['content']?>
                                            </td>
                                            <td>
                                                <?php echo $value['focus']?>
                                            </td>
                                            <td>
                                                <?php echo $value['writer']?>
                                            </td>
                                            <td>
                                                <?php echo $value['view']?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url().'index.php/admin/edit_berita/'.$value['id_news']?>">
                                                    <button type='button' class='btn btn-block btn-warning btn-xs'><i class="fa fa-pencil"></i></button>
                                                </a>
                                                <a href="<?php echo base_url().'index.php/admin/delete_berita/'.$value['id_news']?>">
                                                    <button type='button' class='btn btn-block btn-danger btn-xs'><i class="fa fa-trash" aria-hidden="true"></i></button>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer"> </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('template/footer.php')?>