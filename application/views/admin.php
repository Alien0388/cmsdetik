<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
Berita
</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo base_url();?>"><i class="fa fa-newspaper-o"></i> Berita</a></li>
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Berita</h3>
                        <a href="<?php echo base_url().'index.php/admin/form_berita'?>">
                            <button class="btn btn-info pull-right">Add New</button>
                        </a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="responsive">
                            <table id="example4" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>Image</th>
                                        <th>Writer</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($list as $value){?>
                                        <tr>
                                            <td>
                                                <?php echo $value['id_news']?>
                                            </td>
                                            <td>
                                                <?php echo $value['title']?>
                                            </td>
                                            <td>
                                                <?php echo $value['content']?>
                                            </td>
                                            <td>
                                                <?php echo $value['image']?>
                                            </td>
                                            <td>
                                                <?php echo $value['writer']?>
                                            </td>
                                            <td>
                                                <button type='button' class='btn btn-block btn-warning btn-xs'>Edit</button>
                                                <button type='button' class='btn btn-block btn-danger btn-xs'>Delete</button>
                                            </td>
                                        </tr>
                                        <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box-footer"> </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<?php $this->load->view('template/footer.php')?>