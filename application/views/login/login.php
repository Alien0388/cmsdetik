<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Login Form</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/'?>css/style.css"> </head>

<body>

    <body>
        <div class="login">
            <div class="login-screen">
                <div class="app-title">
                    <h1>Please Login</h1> </div>
                <div class="login-form">
                    <div class="control-group">
                        <form class="login-form" method="POST" action="<?php echo base_url();?>index.php/login">
                            <input type="text" class="login-field" value="" placeholder="username" id="login-name" name="username">
                            <label class="login-field-icon fui-user" for="login-name"></label>
                    </div>
                    <div class="control-group">
                        <input type="password" class="login-field" value="" placeholder="password" id="login-pass" name="password">
                        <label class="login-field-icon fui-lock" for="login-pass"></label>
                    </div>
                    <button type="submit" name="submit" id="submit" value="Login" class="btn btn-primary btn-large btn-block">login</button>
                    </form>
                    <?php if ($this->session->flashdata('gagal')){
           echo "<div class='alert alert-danger' style='margin-top:10px'>
           <strong>Wrong password or username!</strong>".$this->session->flashdata('gagal')."
           </div>";
         }
          ?> </div>
            </div>
        </div>
    </body>
</body>

</html>