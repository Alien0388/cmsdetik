<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        * {
            box-sizing: border-box;
        }
        /* Add a gray background color with some padding */
        
        body {
            font-family: Arial;
            padding: 20px;
            background: #ECECEC;
        }
        /* Header/Blog Title */
        
        .header {
            padding: 30px;
            font-size: 40px;
            text-align: center;
            background: white;
        }
        /* Create two unequal columns that floats next to each other */
        /* Left column */
        
        .leftcolumn {
            float: left;
            width: 75%;
        }
        /* Right column */
        
        .rightcolumn {
            float: left;
            width: 25%;
            padding-left: 20px;
        }
        /* Fake image */
        
        .fakeimg {
            width: 100%;
        }
        /* Add a card effect for articles */
        
        .card {
            background-color: white;
            padding: 20px;
            margin-top: 20px;
        }
        /* Clear floats after the columns */
        
        .row:after {
            content: "";
            display: table;
            clear: both;
        }
        /* Footer */
        
        .footer {
            padding: 5px;
            text-align: center;
            background: #1B3682;
            margin-top: 20px;
        }
        /* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
        
        @media screen and (max-width: 800px) {
            .leftcolumn,
            .rightcolumn {
                width: 100%;
                padding: 0;
            }
        }
    </style>
</head>

<body>
    <div class="header"> <img src="<?php echo base_url().'assets/image/detikpialadunia.gif'?>" width="300"> </div>
    <div class="row">
        <div class="leftcolumn">
            <?php foreach ($list as $value) {?>
                <div class="card">
                    <h2><?php echo $value['title']?></h2>
                    <h5>detikNews | <?php echo $value['date']?></h5>
                    <p><i><b><font size="2px">Oleh : <?php echo $value['writer']?></font></b></i></p>
                    <p>
                        <?php echo substr($value['content'], 0, 500)?>...<a href="<?php echo base_url().'index.php/berita/content/'.$value['id_news']?>">Read more</a></p>
                </div>
                <?php }?>
        </div>
        <?php $this->load->view('berita/mostpopular');?>
    </div>
    <div class="footer"> <img src="<?php echo base_url().'assets/image/logo_detik_footer.png';?>"> </div>
</body>

</html>